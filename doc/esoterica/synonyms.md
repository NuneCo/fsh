Not cattle nor pets, but synonyms.

# Past the Oops and Funks
Somewhere, on the other side of the language wars, the copywrite wars, the social media wars, and the irl wars the bleed over into the network, there's a user with needs unmet.

They might be a [Jessica](@link_swift_on_security:jessica), or they might be a grandparent, a niche professional, or an industry worker.

None of them care whether we code it with vim, emacs, or some other.

None of them care if it has an API or visible/open source.

None of them have ever had their needs met.

The nontechnical is the end user here, and therefor it is assumed that you, the user of this system, is attempting to meet the needs of others, through software/system/infrastructure engineering, and you've come here looking for a perspective shift, or a restructuring of how you do some task, or possibly just to borrow/improve a tool.

Either way, be mindful of the goal.

Not just useable, but accessible tech.

# Why Synonyms?
Because nothing else quite sums up the way a declarative system works, and the pet/cattle distinction is far too narrow, especially when proper backups are concerned.

## Proper Backups?
Something that is synced across at least three points, with an incremental cold storage and automated restore mechanism.

# Using Synonyms
Any system off the repo's `main` branch could be considered a synonym, technically, but it's a user-centric measure of intercompatibility than a technical definition.

Although measures of synonym-ness have yet to be investigated or calculated, **it is expected that a user is able to seamlessly move between physical units considered synonyms**.

Within the DarkPi ecosystem, it is possible for a headless network storage controller, a DJ system, a scientific data collection console, and an MP3 player all share the same `main`, so this word has nuance and depth beyond just "they share the underlying system configuration".

Generally, saying something is a synonym means it exists off the same branch, probably for the same purpose, and often facilitates the same local node, eg a trio of load balancers would be considered synonyms, as would several POV-cam units.
