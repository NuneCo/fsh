@todo - basic repo stats?
total lines/1w/3m
commits daily average in active weeks since repo start, longest streak?
Display devlog/changelog?
Show recent commits with more than one line, timeline of git-commit comments?


#2021.347
could really use the pinglog for some of this work.

#2021.342

Adding personal ssh pubkey, if exists.

#2021.333

#2021.333

	--> create indexing mechanism (done 2021.332)

useful next steps:

file-interface project:
	--> sorting mechanism
	---> define categories & project filters
	---> setup sorting queue
	---> queue actions interface & testing (tdd will be useful to have working by this point)
	----> requeue & undo
	----> move
	----> annotate
	----> transcribe? / caption?
	----> publish?
	----> .
	----> archive
	----> dedupe?
	--> rclone copy (protoype from 2021.31x work, needs polishing & feature pruning, get tdd working on this?)
	--> chunk mover (nano cutbuffer integration w/ abduco&dvtm?)
	--> category / project term search (rehabilitate old function search mechanism?)
	-->

phrase / character replacement? (sed-lite? eg ' & ' w/ ' and ')

fix the namer stack

tools for exfiltrating data (while preserving contextual information & meta-connections) from:
gdocs, imap, (s)ftp, rocketchat, slack, matrix,

commands in an ordered / unordered rotation?
symlink-based multi-point file editer? w/ nano?

anypath (ssh/mosh, lora, bluetooth) system control api w/ auth, online timeout offline jobwatch etc
cross-network hard/fast/graceful shutdown & de-energize

microsite project:
 - systems statuspage
 --> repo w/ basic analytics
 --> userspace
 --> overall usage (graphs?)
 ---> bottlenecks & capacities?
 ---> hardware?
 --> build pipeline
 - calendar
 - (pijul?) repo-based forum viewer w/ per-post publishing granularity?
 -

#2021.309
Following up on the rclone work today.

Need the functionality (effortlessly sync a directory between multiple systems) for work, and the adjacent work-demands mean that I've got an easy test case in the form of my MP3 player.

Although the initial work allowed cross-system sync to work, automating local files seemed like a good first step.

This begins by eliminating a friction - the one-liner to obtain an MP3 - by adding the functionality into the functions file.

#2021.057
git config --global init.defaultBranch main

#2021.054
Pipes don't work with 'fet', whoops >_>

#2021.036
Need to make a git-init-sys command.

Something like...
alias fit="git config --global init.defaultBranch main &&..."

#2021.018

The distinction between:
`wget https://hydra.nixos.org/build/135337873/download/1/nixos-sd-image-20.09.2653.fee7f3fcb41-aarch64-linux.img.zst` (2.8GB)
AND
`wget https://hydra.nixos.org/build/135341821/download/1/nixos-sd-image-20.09.2653.fee7f3fcb41-aarch64-linux.img.zst` (2.74GB)
is the difference between a non-booting, and booting, system.

The (-1) datacard system, representing unstable dev, will have configuration copied to the (+1) card, testing reproducibility.
The (+1) datacard system will be kept as the primary backup for when dev fails, keeping a process to bring another (+1) system online is crucial.

The (±0) datacard is the known-good working system.

20.09.2653 fails to rebuild.
"Out of memory" (ref:media/rebuild_oom_2021.018.jpg)

https://github.com/nix-community

https://github.com/NixOS/nixpkgs/issues/82455
https://nixos.wiki/wiki/NixOS_on_ARM


Trying again with
https://hydra.nixos.org/build/135199450/download/1/nixos-sd-image-21.03pre264254.b7e5fea35ef-aarch64-linux.img.zst

(from https://hydra.nixos.org/build/135199450, via https://github.com/NixOS/nixpkgs/issues/82455#issuecomment-608618064 )

Lots of "WARNING" scrolling past, but nothing has hung or thrown persistent error.

rebuild and test complete, attempting a confirm and reboot.

Which, for ref, is currently 'nixos-rebuild --upgrade switch', although the reliability of the method is not yet to a point where creation of a useful alias is necessary. Though, `alias confirm='nixos-rebuild --upgrade switch'` would make sort of sense.


Practice the dodgy bits on the -1 card.  
Once there's a process that works, re-test on the +1 card.  
Once the process works, use automation to upgrade the ±0 card.


#20201216

Preparing for a fully automated install.

Beginning with the partition.

A 1GB boot partition, using the Raspberry Pi specific bootloader, and heavily referencing this bit from [stackoverflow](https://serverfault.com/questions/258152/fdisk-partition-in-single-line#comment1354587_721878):
"I had to first create the partition table with 'parted /dev/nvme2n1 mklabel gpt' and then create the partition with 'parted /dev/nvme2n1 mkpart primary ext4 0% 100%' After that I formatted it with 'mkfs.ext4 /dev/nvme2n1p1'."

so, 'parted /path/to/device mklabel gpt' prepares the disk (it asks for user input, @todo script unattended usage).

First objective: to automatically get the device name.

'parted -l' gives a list of hardware, and grep/awk are useful tools here.
The raspberry pi system currently under keyboard uses '/dev/mmcblk#' nomenclature for the builtin SD card reader, and '/dev/sda' for the first attached disk.
As this conflicts with many Linux device designations, detection of target becomes selection of target if the install path is clear, otherwise prompting the user for corrective action.

Having manually run through the partitioning and nixos install commands, something is missing, and the system does not boot.
Using 'dd' has previously produced working, though often erratic in odd ways, systems...but this seems less than ideal.
Using prebuilt images switched to a new configuration has worked, but adds a remote dependency for images.

Need to determine the rpiv4 sd install path, and figure out the contribution pipeline.

#20201207

In response on the fediverse:
My cooperative survives. That's our function and purpose, and what we build is first oriented towards that goal.

The wiki pages (https://gitea.com/nuneco/irregular.team/wiki/) are mostly orienting around the fiddly bits of keeping our sort of group cohesive, copacetic, and communicating...and still need work to be ready for public consumption, so please ingest with a grain of salt.

Our goal is to benefit others, too...and in small ways, it has. I've laid out some of the more abstract and long term goals via our liberapay: https://liberapay.com/nuneco/

(and a friendly is working on a cohesive 'what we're about" for the main team page, to help with this sort of query...)

---

That said, we're a tech org. I do build system & automation, and the project I was referencing (with the weather station) is indeed a use of the underlying hardware/software paradigm (see https://darkpi.com/ for the out-of-date project stuffs).

I'm currently attempting to package said project into a package that'll get us some form of funding...putting the least-useful bits into something that'll let us pay the rent, and hoping that capitalism as usual doesn't rip it to shreds and steal the benefit before we can make it available for others to us.

---

It's hard to describe what this base system is about.

It's about decentralizing all the things necessary for building more, identical or variant, systems.

Our hardware is currently the Raspberry Pi with a LoRa backpack for sensors and human interfaces. Any base system can write-out itself to an SD card, and any system can build other systems if there is a copy of said system on the network.

Most/all of this is prototyped in Bash chunks, carefully stashed somewhere, and the base system needs a year ish of dev work to get to a "working alpha" point, regarding our core deliverables.

---

Regarding how you might fit in...have you played with NinjaTek's Armadillo material (https://ninjatek.com/armadillo/)?

I'm hand-carving pieces for the kinesthetics of the physical interfaces, but iterating 3d printing is the goal once we've got the base design and hardware layout set.

This is, ultimately, a platform for proper augmented reality that we're building (see the darkpi history page).

A local-first, cloud-agnostic, user-empowering, consent-based, energy efficient platform for anyone to turn their environment, their contacts list, and their professional needs into a specific configuration, and share the (polished or unpolished) configuration with others individually, privately in a group, or publicly for anyone.

I'm pursuing this, personally, because when my child is old enough to want to learn about electronics, I want a device that is safe to give them...and there isn't anything that currently meets even our initial thoughts (https://gitea.com/nuneco/irregular.team/wiki/protected) on protected information bits...much less the many ways that data can violate consent of a minor.

Regardless of whether the hardware runs as an augmented reality dev platform, a glorified media centre, or a remote weather platform, it's the same core hardware underneath, and about 85% of the underlying software too.

The GPS implementation is just a GPS module paired with offline maps. The MP3 player is an audio HAT with hardware buttons for media control (and any system on a local network can pair with any system, as a remote or as a media source or...). The contacts server syncs with existing mobile and stationary computers, and if mains power goes off, the base station provides a wireless network for user and system use.

We're creating this with the to allow anyone to offer their unused system capabilities to anyone they share a network with...if you're building/hosting something big, or blocking a specific sort of spam, or need to crunch a lot of data, or need to stash data while moving an office, you can ask your trusted network*s) for help with the task(s), and the queue will distribute it to any open systems (using named data networks for this, in theory).

We're pursuing this with intention of installing them in our houses, in our vehicles, and in our pockets and purses...putting something we, the users, own completely between us and the parasitic grasping of current tech.

===

This got longer than intended, but it's mostly my own engineer brain going "eeeeyyyyyy we've been needing input from someone like this, better wordbarf it all out so they can pick out the bits that interest them".

Does that help a bit to clarify what our 5-10 year plan is?

#20201120
Dumping thoughts on #devops in here, because email is objectionable to brain for some reason at this moment in time:

Current structure for publishing via the server is based on username - in that you have a home directory space in `/home/${USER}/`, and a public site at `${USER}.domain.tld` with associated folder under `/var/www/public/${USER}`.

This system automatically handles everything except port forwarding, which is currently limited to autoconfiguration on my own router, but the information to configure most routers is readily available.
This allows each person to decide if they want to host locally (for their subnet only, or visible externally through port forwards) or develop/write locally, and have the mesh host their static files.
The idea being, a single device (raspberry pi) can handle serving static pages easily (though we need data on how many requests a single unit can satisfy), and renewable energy solid state hardware is very efficient (compared to 'traditional' servers). Woozle and myself and several others have been discussing how a volunteer-based CDN, where a mesh of mutually-trusting individuals provide external accessibility to RSS feeds, microsites (blogs or documentation, etc), and other static items.

Because the devices used to serve static assets are also the communications interface, task tracking, and inventory systems, it allows people to opt into eg a grocery share or tool library. We're developing this with the intention of giving away raspberry pi zero systems with an optional clip-on display, so anyone with access to USB power can connect to nearby nodes, or to other nodes via internet link. A lot of the functional design around this is from my time living out of my car, and the solar & wind recharging packs are very much designed for nontraditional housing circumstances.

Our USB3/C (RasPi4B4GB) portable server has a 9 hour battery life from 100% (Redbrick provided 2,322mah over 9h22m before automatic cutoff).
Am curious to see how that baseline extends as I pare the hardware down with software controls, eg use the arduino to time poweron/sync based on battery levels, and also how that compares to the minimalistic hardware (PiZeroW) running on the same battery, as well as information on 'normal usage' eg with a display connected & draining the same battery.

---


#20201119
Bridge online again, with a more optimized path to upgrade.

Having more than one SD card in the loop definitely helps.  
In a way, burning down the bridge was the motivator for progress.

Yay for digital pyromania, eh?
=======

@point-in-time working assesment

> Currently, what is working?

loading the shim from .profile or .bashrc.  
using `frl` to reload the shim.  
using `feq` & `kd` to effectively end troublesome programs.  
using `upgrage` & `shutgrade` to manage Debian system updates.  
using `fet` to show git & kanbash progress.  
using `fd` & `fds` to enable rapid file queuing process.  
using `fds` to enable publishing of changelog bits.  
using `ff` to jump into fsh dev mode.  

There's probably other bits.

> remembered:  

auto-queuing of the devlog for commits.  



@update this? @wikify this?

#20201114

Got the main installation path working, averaging 20-30 minutes on a Pi 4.

Also burned `bridge` down by running `grep` on the root filesystem, as root.

...won't be doing that again.


#20200930

Pursuing wireless config.


#20200919

Successfully tested a three-display synchronized editing session, via local, bridged, and local network connection.

The process will work, in theory, for any bash-compliant-ish use case, and scaling above three systems is untested and assumed to bottleneck quickly.


#20200904

Sometimes, the function of this codebase is a space to tinker, but increasingly there's a need behind the pastime.

The functional need is for a stable, structured, backed-up build system, a NAS, and a straightforward pipeline for additional projects, moving forward.

A scaleable framework.

---

Yesterday, the reverse shell from the dev laptop to the public-facing bridge was established. I/we say public facing to mean something that is visible, but private, at least here.

This reduces friction between on-site and portable computing, and reduces the need to move backend nodes to a given subnet before use. It also removes blockers for creating a config for NixOS with a reverse shell, because a working example was used manually, so it can be copied for the automated node's connectivity. The goal is still to integrate Yggdrasil (and move some/all of this code to python 3, while we're at it), but that's long term, and for three sites, a reverse SSH is enough.

With the `development` branch running on a test/local machine, integrating changes from `dev` branch is next.  
Once `dev` branch is integrated, the applications prototype for Nginx is next.  
Once nginx is done, building the tunnel/sync/messaging stack is next.


#20200902
A recent blogpost, found while refining an infrastructure process, offers a [route to building custom images for NixOS](https://rbf.dev/blog/2020/05/custom-nixos-build-for-raspberry-pis/), but it left me feeling a bit less than satisfied.

What I want is a script that I can point a vultr instance at, and have the whole thing come online automagically.

Kinda low-key decided this process, of making a useably set-up system would be be nontrivial, which is what leads me to the current stack.

The raspberry pi hardware was chosen because of the massive availability of the platform, and the cloud provider vultr was chosen as a less-objectionable option compared to amazon.

Things get a little bit more nuanced at the above-platform layer.

The NixOS system solves several key computational maintenance headaches, primarily updates and consistency. In essence, there's a file (or set of files) that defines everything about the system, such that a completely new, identical system, can be produced at any given time.

While excellent in theory, the implementation has some flaws, noteably that rolling back to a known good configuration tends to be nontrivial, and like most things it's easy to accidentally screw it up.

That said, it's the most straightforward and stable methodology for system maintenance that we've encountered so far, so, here we are.

---

Here's my problem tho.

There's a lot of ways to approach this problem, and it's difficult to figure out which ones work without trying them.

===

Got the dev server verified on the development branch, but there's changes from dev to merge in.

Another day, though.


#20200824
...figured out a core problem with my mentality, and a blocker in scaling my local system up to a real mesh...distinguishing between server and client (every client is a server, and every user represents a node on the net, at any functional point in time).

Interestingly enough, it was while deciding on a hostname for my development machine that it clicked.
Assuming that the local system is just a client, limits significantly.

#20200823

Slowing down, documenting, and planning today.

No rest for the wicked.

Attempting to actually use the script is on the way through the assembly pipeline.

#20200822

slight rant on this as I refactor away some cruft.
these two commands do very different things, in subtly different ways.

`alias fra='echo "${fSrc}"'``
'" #&& sleep "0.1" && echo "aliases reloaded."'
`alias frl="echo \"${fSrc}\""`
" #&& sleep "0.1" && echo 'fsh reloaded.'"

the first command ('fra') pulls the variable FSH_ALIASES at the time of aliases.src sourcing.
the second command pulls the variable from the working environment at the time of alias usage.

so, they both are useful, but the implications are nontrivial and subtle.
if, for example, the variable changes between the time the file is sourced and command usage,
it can either be a lifeline to a known-good configuration, or a frustrating persistence that delays expected outcomes.

that is just *one* distinct way in which bash can fork you over,
and there's several more in *just* this one command,
because the underlying principle (interpreted at runtime or stored at sourcetime)
carries past variable instantiation.

for example, this will echo out the entire source of the file, like 'cat':
`alias fra='echo ${fSrc}'`

keep in mind, the goal here is usable shortcuts, not esotericism.
explaining this walks you through how to solve a problem, tho.

...and this script set attempts to solve many problems before they form.


#20200821
> cleanrun, continued (2/?)

Took a break to microdose some carbs and terpenes.
Balancing the THC with CBD today, like lucasoil for the brain.

Renewed focus post visits from other coop members.

#20200821

Attempting another clean run-through today.

Merging the [vultr docs](https://www.vultr.com/docs/install-nixos-on-vultr), a quick-and-dirty intro to `git` workflows, my ditch-wizard knowledge of NixOS, and an emergent need for the ability to do stuff in collaboration with others towards...some form of progress.

Starting with threshold bits.
Using vultr gives us a straight shot at installing NixOS, but it's a powerful platform that i/we've just started scratching at the surface of.

adjacent: Still need to figure out the [serial console on the raspberry pi](https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi#HDMI_output_issue_with_kernel_5.4_.28NixOS_20.03_or_NixOS_unstable.29), especially for the luggable.

Currently shoving [Discord](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) and [Gitea](https://docs.gitea.io/en-us/webhooks/) at each other in the context of [git post-commit hooking](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) to clear a low threshold on something.

Gitea is showing...errors?...in the gitea hooks [recent deliveries log](https://gitea.com/jakimfett/fsh/settings/hooks/328), but nothing shows up in the chat for that period.
An [alternate testing tool](https://www.lew.la/discord/) [works](https://discordapp.com/channels/469887577311150080/469890007579099146/746479906614935552), so it's not a Discord thing (this time...).

Having your operations, your automation, be [per-repo (and unsync'd)](https://stackoverflow.com/a/12222225) seems like a flaw, actually?

Developing with git is like driving stick shift, where every moving part has three settings that you've got to manually tweak in realtime to keep the system running, and you keep finding *new* bits that need monitoring...

...like [deleting branches locally and remotely](https://www.freecodecamp.org/news/how-to-delete-a-git-branch-both-locally-and-remotely/).

...[this one](https://pc.net/helpcenter/answers/backslash_vs_forward_slash) always confuses me.

#20200728
An example of the previously-mentioned functionally impacting current workspaces:
> My laptop's operating system, Linux Mint, just recently updated
> from v19.x =-> v20, and there's a long list of things to do to ensure your
> system doesn't break.
>
> currently purchasing NAS parts so I can do backup, how's your day going?
> ~ jakimfett

These steps are necessary, but take time.

One month should, in theory, be enough, but this is only if all these threads are kept on track.

When profession begins intersecting with hobby, and the combination becomes necessary for progress towards survival goals, sometimes you eat the cost, go with the good option, and know you're protecting your future path, or the future path of others, against unnecessary digital decay.

#20200726
One of the big barriers to substantial amounts of implementation is the lag between "there is time to work on this" and "things are set up and ready to be worked on".

Each reboot, every login timeout and reauth, every IP & DNS change, all these barriers between desire and outcome reduces the spoons available to work on the actual core of this project, the automation of all the painful bits.

Some days, all the spoons are eaten just trying to keep the internet stable for usage.

#20200722
find -maxdepth 3 -type d | grep -v git >> doc/esoterica/dev.log

There's a balanced structure that seems almost within grasp.
That above command produces a list of the folders within the project, from which i/we must distil the essential.

```
.
./stack
./cfg
./doc
./doc/esoterica
./doc/platform
./doc/platform/raspbian
./doc/platform/nixos
./doc/platform/routerOS
./core
./tmp
```

Is tmp a necessary? Are doc and cfg?
...could doc & config be merged with core?

hmmm...core/config, that distinction starts getting difficult to discern.

What about...stack, doc, and tmp?
Would that meet the need?

---


got that merged into dev.

Probably broke a lot of...something.

Oh well, time to find where all the magic paths are, ya?

Using the dev laptop as a host, and the tablet as a thin client (plus git-relate extras) seems to work. Workflow involves three repositories, with the src/ repo on the laptop as the intermediary between the two workspaces, devlog on tablet and code/testing on the laptop.
