Obfuscation of function, competence blindness, and the rise of inaccessable technology.

# In the beginning...
...was the command line, obviously.
_(the voice interface was a bit later)_

Consider that now, without knowing keywords or elevated knowledge, the most successful software in the world cannot be used without extensive training.

Consider: `sound` vs `audio` vs `pavu`.

These all, functionally, mean the same thing...getting at the auditory settings for the operating system.

To find this, you must search (with applications able to interrupt or pull focus) in a bar on a specific display (pray you turn on the displays correctly, multimonitory support is still flaky at best), and for the advanced settings (which is where you can actually control the fiddly bits of software defined audio), you need to know of the existence of Pulse Audio Volume Control.

Now, my frustration with the opaqueness of "modern" 2020 Linux (and the niche uses therof) is multi-fold.  
Installation is complex, and requires technical decision-making competence.  
Testing requires confidence in working outside the lines, and delving deeper than the menues brings you right back to the command line.

And the proprietary operating systems are growing noticeably *worse* with each iteration, so end users are left without options.

They spend. And spend. And spend yet again.

The code you write fuels the engines of tomorrow's systems.

Be judicious.

Be efficient.

Be discreet.
