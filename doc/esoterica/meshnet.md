The public-facing server, or frontend, serves https traffic, and provides public-ish SSH and SFTP entrypoint for locale-specific mesh'd hardware.

# Backend

Each node on the mesh will be a build server.
Each node connects via the public entrypoint (initially) and via the Yggdrasil network when possible.

Gemini? Masto/Soc? etc?
