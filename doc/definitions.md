"Eventually, we had to start defining things, or our intentions became lost." ~ unknown

# Why
Because sentients on this planet tend to squabble less if we lay out the meanings ahead of time.

@format "word/phrase/defineable" = meaning of the term in [simple](@link thing explainer) language


"consensustructure" = an infostructure optimized for group consensus at scale.
"infostructure" = infrastructure for structured information
"synonym" = a similar, but differing, information structure.
