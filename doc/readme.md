For a decent experience with this software,
you need a base level understanding of Linux shell scripting & [git revision control](https://eliotberriot.com//blog/2019/03/05/building-sotfware-together-with-git/),
and most likely a Debian-flavoured system to run things on top of.

This time around, there's an attempt to cut back on the levels of [Heavy Wizardry](http://www.catb.org/jargon/html/H/heavy-wizardry.html) in use.


# Installation

Pick where to put the code.
`mkdir -p ~/src/fsh ~/bin`

Get the code.
`cd ~/src/fsh && git init && git remote add origin https://functions.sh/ && git fetch origin && git merge origin/dev && git checkout dev`

# Interaction

Defaults are non-destructive, see `f --help persist` for writing to disk.

### Examples:
_get dependencies of an installable_
`f --depends <software(:sym.ver.sion)/>`

_check installation prerequisites/dependencies for nginx_
`f --install nginx`

# Assumptions
Some structure is assumed, and some structure can be customized.

Currently the live code is expected under the `~/run/` directory, and bare git repositories are expected in the `~/src` directory.

 
