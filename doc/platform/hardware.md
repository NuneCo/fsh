Getting it right isn't enough, there's an aesthetic to it that must resonate.

# Kinesthetic Biofeedback
The feel of an object, a device, in your hands, should be right.

There's a flatness, a lack of texture, that the 2020-ish iteration of thirty-to-seventy years of electronics "improvements" have achieved.

The obvious differences between the potential of [1968](https://en.wikipedia.org/wiki/The_Mother_of_All_Demos), and the reality of 2020, have never been more distinct to me than while looking at the functional requirements for our [communications project](wiki/secure_collaboration.md).

We've many bins of digital building blocks, but nothing that exists feels right.

The tech sector, among other toxic wastelands such as corporate politics, startup culture, and the social-cultural erasure of the Kiowa people has led me to this place where chasing a feeling with software is my best shot at saving my friend's lives.

I don't know what the second half of this year,  
or even what _tomorrow_,  
might bring my little family.

What I know is that when there's an iteration of this device that feels good to me, something modular and more-than-robust enough to hand to children or grandparents, and yet still allows me an interface for serious development of the selfsame system, whenever and wherever...

...then we are approaching the first goal.
