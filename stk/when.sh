#!/usr/bin/env bash
# returns a precise & human readable date format

# double line
date +"%j day %t%t%t (y:%Y - m:%m w:%V d:%d)%n%H:%M.%S chrono%t%t%t%t   (%Z/UTC%z)"


# with day atop, first
#date +"Day %j %t%t%t chrono: %H:%M.%S%n(y:%Y - m:%m w:%V d:%d)%t   (%Z/UTC%z)"
# with year atop
#date +"y:%Y m:%m d:%d %t%t chrono: %H:%M.%S%n(Week %V, Day %j)%t%t   (%Z/UTC%z)"

# single line
# date +"y:%Y m:%m d:%d (Week %V, Day %j)%t%tchrono: %H:%M.%S (%Z/UTC%z)"

