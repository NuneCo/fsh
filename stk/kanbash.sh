#!/usr/bin/env bash

# config file name
CFGNAM="kanbash.conf"
DATNAM="kanbash.csv"

BASDIR="$(realpath ~/ )/.config/kanbash/"

function logThis {
	echo -e "${@}"
}

# fsh version of kanbash
function kanBashHere {

	local confPath="${BASDIR}${CFGNAM}"
	local datPath="${BASDIR}${DATNAM}"

	local confFound=0
	local datFound=0

	if [ -f "${confPath}" ]; then
		logThis "found conf!"
		confFound=1
	fi

	if [ -f "${datPath}" ]; then
		logThis "found dat!"
		datFound=1
	fi


	export KANBANCONF="$(pwd)/kanbash.conf"
}


logThis "Folder: '${BASDIR}':\n\t\t'./${CFGNAM}'\n\t\t'./${CSVNAM}'"
