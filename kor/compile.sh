#!/usr/bin/env bash
#
# This is a prototype, use discouraged.

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euox pipefail
else
    echo "These scripts require 'bash' version 4.4 or greater, your version is insufficient:"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar


clear
if [ -f "kor/$(basename $(realpath ${1}))" ] && [ "$(basename ${1})" == "aliases.src" ];then

	# enable alias expansion, load the aliases file
	shopt -s expand_aliases && . "core/$(basename $(realpath ${1}))"

fi
. "core/$(basename $(realpath ${1}))"
logThis "Working directory is '$(pwd)'..."
logThis "...passed in '${1}' from command line arguments..."


function recompile {
	echo "recompiling..."
	sleep 3
	exit 13
}

trap recompile EXIT


# @todo - tests for install, purge, compile
# @todo - spawning and reaping. disownership.
# @todo - system service?
# @todo - loggint parity refactor
# @todo - eliminate magic paths
