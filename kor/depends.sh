#!/bin/bash
#
# Accepts a pointer to a text file of dependencies, and attempts to install them.

# Commands help, short and long
declare -A env config enabledCommands commandDescript sanParam

enabledCommands['d']="dry-run"
commandDescript["${enabledCommands['d']}"]="Checks for dependencies, and displays the command to manually install them."

enabledCommands['i']="install"
commandDescript["${enabledCommands['i']}"]="Installs the dependencies to the local system."

enabledCommands['p']="purge"
commandDescript["${enabledCommands['p']}"]="Attempts to remove all traces of dependencies from the local system."

## include f.sh ##
shopt -s expand_aliases;source "$(realpath "${0}" | sed 's|\(.*\)/.*|\1|')/f.sh";logb
## end includes ##

logBreak

# variable structure
declare -A depsPresent
declare depList

# Default source for the dependencies file
config['depFile']='conf/functions.deps'

# @todo - parse line input

# load deps from file here
# check deps
# install if --install or -i is set

function getDependencies {

	mapfile -t depList < "${config['depFile']}"

}

function compareDependencies {
	for (( i = 0; i < ${#depList[@]}; i++ )); do
		isPresent=$(dpkg -s ${depList[i]} 2>/dev/null | grep installed | wc -c)

		if [ "${isPresent}" -ne 0 ]; then
			depsPresent["${depList[${i}]}"]=${depList[i]}
		fi
	done
}

function checkDeps {
	if [ ${#depsPresent[@]} -ne ${#depList[@]} ]; then
		echo "Dependencies found: ${#depsPresent[@]}/${#depList[@]}"
		echo

		exit 1
	else
		logThis "All dependencies present." 1
		logThis "(Dependencies checked:"
		logThis "\t'$(echo ${depsPresent[@]})'"
	fi
}

function installDependencies {
	echo "Attempt installation of missing dependencies..."
	toInstall=""
	for (( i = 0; i <= ${#depList[@]}; i++ )); do
		if [ "${#depsPresent[${i}]}" -eq 0 ]; then
			toInstall+=" "
			toInstall+=${depList[${i}]}
		fi
	done
	installString="sudo apt install -y ${toInstall}"
	echo "(using '${installString}')"
	echo
	echo "Input superuser password to continue installation:"
	eval ${installString}
}

# Require the functions litany here
function require {
	hash "$@" || exit 127
}

require "bash"


# if no parameters have been passed, display the help
if [ "${#sanParam}" == 0 ]; then
	usageHelp
	exit 0
else
	checkDeps
	echo ${sanParam[@]}
	exit 0
fi
