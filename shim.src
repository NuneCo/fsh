#!/usr/bin/env bash
# hashbang ignored if sourced, but important for execution stability.
#
# theory - code supporting documents, like a cradle of infrastructure.
#
# # @todo generate entire shim via concat of requested features?

# ...this is going to need to be an associative array, isn't it.
# ...which means there will need to be a linking loop, so each location gets properly checked (for existing, etc)
# ...so a refactor of 'shimState' and 'shimMe'/'unShimMe' are in order, too.
# ...plus side, post-refactor, there will be a pretty decently decoupled set of functions to use, eh?

install='.bash_aliases' # <-- @todo this needs to accept a list of files (or append to the defaults?
# ^-- that's it for configuration (relative to ~/)
#
#	(change the @default if you're making a source change)
#
# the rest of this is basically boilerplate --v

# @todo - organize
#	- hashbang
#	- user editables
#	- includes
#	|--> functions first, obviously
#		|--> @todo determine: can we source/link from within linked files,
#		|	and still have viable functions/variables in the userscript space?
#	--> environment variables (move into functions?)
#	--> aliases (move into functions?)
#	--> bash qol? merge/differentiate?
#	-->
#	- shim/unshim functions last?
#	- shim checker?
#	- exit trapping?
#	- source/execution paths?
#	-


### enabling shim stuffs ###
#
## tl;dr ##
# search for **and understand** alias usage before changing things.
#

## less terse version ##
#
# Modifying this aliases file has deep repercussions in the user experience,
# *hence* the minor wall o' text
#
# Installing, reinstalling, and purging are essential program basics.
#
# note upon this refactor: fsh still cannot self-install,
#	but the process works for nearly all the of the initially documented edge-case failures.
#

# Determine if the script is being executed, or sourced.
if [ ! -z "${0}" ] && [ ! -z "${BASH_SOURCE[0]}" ]; then

	# If both of those variables are set AND equal eachother, the file is (probably) being executed.
	if [ "${0}" == "${BASH_SOURCE[0]}" ]; then

		echo "Try sourcing this file instead, eg '. ${0}' or 'source ${0}'."
		# if executed, exit
		# just exit for now, attempting to source produces a loop which becomes a segfault fairly quickly.
		exit 13 # <-- @todo document the exit/return status stuffs somewhere

	# my father solved this lack-of-automation mistake once, so i/we can too, right?
	#echo "(...doing it for you this time, tho :-P)"
	#source "${BASH_SOURCE}" && echo got here
	# (debug usage tbd)
	# something went wrong
	#echo "failmuffins: could not source '${0}'"
	fi
fi
# (normal usage)
# v-- sourced

# Allows alias use inside this script, both via sourcing and executed directly.
# @todo - determine how far this stacks/recurses
shopt -s expand_aliases

# load the shim filepath into a variable for later use
export fSrc="$(realpath "${BASH_SOURCE}")"

# derive the folder from shim filepath
export fDir="$(dirname "${fSrc}")"

# Source the rest of the environment variables
. "${fDir}/cfg/env.vars"
# @todo move all non-install-related variables to the env.vars file


# Source bash QoL stuffs, mostly legacy now.
# @todo - identify and install ElementaryOS's case-insensitive tab completion
. "${fDir}/kor/bash.src"

# Source the functions file
. "${fDir}/kor/functions.src"

# Source the aliases
. "${fDir}/kor/aliases.src"

### load containerized stack features ###
. "${fDir}/stk/logThis.src" # @provides dated logging and shell visibilty
# @todo figure out a way to embed functions into single file without repeating myself all over the place

# Source any previous aliases file, avoid duplicates.
# @todo - eleminate the remaining magic paths (and/or magic extensions)
if [ -f "${fLink}.old" ]; then
	if [ ! -L "${fLink}.old" ]; then
		echo
		echo "loading previous aliases from '${fLink}.old'"
		. "${fLink}.old"
	fi
fi

echo # <-- whitespace is important too, you know

shimState

echo

# show user the chronostamp
eval "${fDir}/stk/when.sh"

echo

# in an attempt to stay self-contained,
# this shim intentionally overrides the sourced 'shimMe' function.
#
# @provides:install
# @modifies(@default):
#	~/.bash_aliases
#	~/.bash_aliases.old
function shimMe {

	logThis "fLink: '${fLink}'"
	# check if the install location is already a link
	if [ -L "${fLink}" ]; then
		echo "Found symlink at '${fLink}', testing..."

		# @todo - fix the problem with realpath not liking the string encapsulation here, cross-check with linter maybe?
		if [ "$(realpath "${fLink}")" == "${fSrc}" ]; then
			echo -e "...outcome:\n\tLink already installed, points to '${fSrc}'"
			return 0
		else
			echo "symlink incorrect:"
			echo -e "\tfLink: '${fLink}'"
			echo -e "\tfSrc: '${fSrc}'"
			echo
			echo "debug plz"
			return 1
		fi
	fi

	# check if the install location is a file
	if [ -f "${fLink}" ]; then

		mv "${fLink}" "${fLink}.old"
		# backup any existing aliases
		echo "moved existing file to '${fLink}.old'"

	fi

	# Set up a link, but only if your environment knows where the fdir is.
	if [ -f "${fSrc}" ]; then

		ln -s "${fSrc}" "${fLink}"
		# link the source file to the install location

		if [ -L "${fLink}" ]; then
			# double check that we're installed

			echo "link installed"
			# alert the user

		fi

	fi

}

#
# inverse of shimMe
#
# if the install file exists and is a link to the existing fSrc
# link is removed, and if an .old file exists,
# replaces it.
#
# @provides:install.invert
function unShimMe {

	echo "checking for link at '${fLink}'..."

	if [ -L "${fLink}" ]; then

		echo "...link found..."

		if [ "${fSrc}" == "$(realpath "${fLink}")" ]; then
			echo "...link matches 'fSrc' location, removing link..."
			rm "${fLink}"

			if [ ! -f "${fLink}" ]; then
				echo "...removal successful."

				if [ -f "${fLink}.old" ]; then
					echo "backup file found, replacing..."
					mv "${fLink}.old" "${fLink}"
				fi

			else
				echo "File at '${fLink}' could not be removed, plz debug."
				return 1
			fi

		fi
	fi
}

## === ##
#
# ~ @copy;/© ????-2022 @jakimfett, all rights reserved.
#
# = #
#
## orphans

# beam us in, scotty

#runningDirectory="/home/${USER}/run/"

#if [ -f "${runningDirectory}" ] ; then
#	cd "${runningDirectory}"
#fi
