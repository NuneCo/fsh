#!/usr/bin/env bash

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar

# === #

dl_dir=~/run/dl

sd_img_url='https://hydra.nixos.org/job/nixos/release-20.03-aarch64/nixos.sd_image.aarch64-linux/latest/download-by-type/file/sd-image'
sd_img_new_krnl_url='https://hydra.nixos.org/job/nixos/release-20.03-aarch64/nixos.sd_image_new_kernel.aarch64-linux/latest/download-by-type/file/sd-image'

# indexed array, for looping
declare -A sd_img
sd_img['stable']="${sd_img_url}"
sd_img['new_kernel']="${sd_img_new_krnl_url}"

if [ ! -d "${dl_dir}" ]; then
	mkdir -p "${dl_dir}"
fi

pwdir=$(pwd)

for img in ${sd_img[@]}; do
# @todo - get the array key, save file as "nixos_${key}"
	cd "${dl_dir}"
	echo wget "${img}" -O
done

cd "${pwdir}"

echo "done."
exit 0
