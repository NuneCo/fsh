#!/usr/bin/env bash

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar

# done templating

# @todo - sync via reverse ssh
remotePath="~/src/fsh"
localPath="user@bridge.local:src/fsh"

rsynCommand='rsync -e "ssh -i ~/.ssh/id_ed25519" '
rsynCommand+='--verbose --checksum --perms --partial --executability --times '
rsynCommand+='--compress --human-readable --progress --recursive --dry-run '
rsynCommand+="'${remotePath}' "
rsynCommand+="'${localPath}' "

#echo -e "executing:\n${rsynCommand}"
#mkdir -p "${partialDir}" &&
eval "${rsynCommand}"|| echo "sync failed...exit was ${?}"




# rsync -e "${SSH}" --verbose --checksum --perms --partial --partial-dir="${partialDir}"
#--executability --times --preallocate --compress --human-readable
# --progress --recursive "$(realpath ${localPath})" "${remotePath}"
