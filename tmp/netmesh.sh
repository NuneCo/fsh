
# this requires root access...why?
# nmap -sP 192.168.1.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'


# stacking for workflow
echo "Pushing to local repo..."
git push local
echo "...entering source folder..."
pDir=$(pwd)
cd ~/src/fsh.git/
echo "...remotes are:"
git remote -v
echo "Pulling..."
git pull
echo "...pushing..."
git push bridge dev
git push bridge 
