#!/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar

# Variables - for tweaking things in manageable ways.
remoteServer=""



/bin/bash jobs/laminar.before && /bin/bash jobs/laminar.run && /bin/bash jobs/laminar.after

