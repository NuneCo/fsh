You'll need an `ed25519` pubkey and a terminal.
(ssh-enabled, mosh preferred)

# script

There's a script available via `curl`/`wget`/`etc` at:
```
https://functions.sh/quickstart.sh
```

You can either download, verify, and run it yourself, or use the lines we prepared:
```
wget https://functions.sh/quickstart.sh -O functions_quickstart.sh
chmod u+x functions_quickstart.sh

```

## keygen

To generate a key, use:
```
ssh-keygen -t ed25519 -a 512
```

